      subroutine nplotter_Z_only(p,wt,wt2,switch)
      implicit none
      include 'types.f'
c--- Variable passed in to this routine:
c
c---      p:  4-momenta of particles in the format p(i,4)
c---          with the particles numbered according to the input file
c---          and components labelled by (px,py,pz,E)
c
c---     wt:  weight of this event
c
c---    wt2:  weight^2 of this event
c
c--- switch:  an integer:: equal to 0 or 1, depending on the type of event
c---                0  --> lowest order, virtual or real radiation
c---                1  --> counterterm for real radiation
      
      include 'vegas_common.f'
      include 'constants.f'
      include 'nf.f'
      include 'mxpart.f'
      include 'cplx.h'
      include 'histo.f'
      include 'jetlabel.f'
      include 'outputflags.f'
      include 'ewcorr.f'
      include 'energy.f'
      real(dp):: p(mxpart,4),wt,wt2
      real(dp):: yrap,pt,yraptwo,pttwo,r
c---  Z->e+e-(31) or b bbar(33): both measured, rapidities and momenta of 3 and 4 can
c---  be calculated, also the invariant mass m34
      real(dp):: y3,y4,y5,y34,pt3,pt4,pt5,pt34,m34,r35
      real(dp):: L0, A0, A1, A2, A3, A4, A5, A6, A7, phi, sintheta
      real(dp):: costheta,p3(4),p4(4),p34(4),wt_ew
      real(dp):: sgn, p1(4), p2(4),boostedZ(4),boostedE(4),boostedP(4)
      real(dp):: CSAxis(3), yAxis(3), xAxis(3), dot3, angle, tmp(3)
      real(dp):: phietastar, delphiaz
      integer:: switch,n,nplotmax, nnew
      integer tag
      logical, save::first=.true.
      common/nplotmax/nplotmax
ccccc!$omp threadprivate(first,/nplotmax/,y4,y5,y34,pt3,pt4,pt5,pt34,m34,r35)


************************************************************************
*                                                                      *
*     INITIAL BOOKKEEPING                                              *
*                                                                      *
************************************************************************

      if (first) then
c--- Initialize histograms, without computing any quantities; instead
c--- set them to dummy values
        tag=tagbook
        y3=1d3
        y4=1d3
        y5=1d3
        y34=1d3
        pt3=0._dp
        pt4=0._dp
        pt5=1d3
        pt34=0._dp
        m34=0._dp
        r35=1d3
        jets=1
        L0=1d3
        A0=1d3
        A1=1d3
        A2=1d3
        A3=1d3
        A4=1d3
        A5=1d3
        A6=1d3
        A7=1d3
        goto 99
      else
c--- Add event in histograms
        tag=tagplot
      endif

************************************************************************
*                                                                      *
*     DEFINITIONS OF QUANTITIES TO PLOT                                *
*                                                                      *
************************************************************************

      y3=yrap(3,p)
      y4=yrap(4,p)
      y34=yraptwo(3,4,p)
      pt3=pt(3,p)
      pt4=pt(4,p)
      pt34=pttwo(3,4,p)
      m34=sqrt((p(3,4)+p(4,4))**2-(p(3,1)+p(4,1))**2
     &         -(p(3,2)+p(4,2))**2-(p(3,3)+p(4,3))**2)
      
c--- phietastar (c.f., for example, Eq. (1) in 1512.02192)
      phietastar=tanh((y3-y4)/two)        ! cos(thetaetastar)
      phietastar=sqrt(1d0-phietastar**2)  ! sin(thetaetastar)
      phietastar=phietastar*tan((pi-delphiaz(3,4,p))/two)


      if(jets > 0) then
         pt5=pt(5,p)
         y5=yrap(5,p)
         r35=R(p,3,5)
      else
         pt5=-1._dp
         y5=1d3
         r35=1d3
      endif

************************************************************************
*                                                                      *
*     FILL HISTOGRAMS                                                  *
*                                                                      *
************************************************************************

c--- Call histogram routines
   99 continue

c--- Book and fill ntuple if that option is set, remembering to divide
c--- by # of iterations now that is handled at end for regular histograms
      if (creatent .eqv. .true.) then
        call bookfill(tag,p,wt/real(itmx,dp))  
      endif

c--- "n" will count the number of histograms
      n=nextnplot              

c--- Syntax of "bookplot" routine is:
c
c---   call bookplot(n,tag,titlex,var,wt,wt2,xmin,xmax,dx,llplot)
c
c---        n:  internal number of histogram
c---      tag:  "book" to initialize histogram, "plot" to fill
c---   titlex:  title of histogram
c---      var:  value of quantity being plotted
c---       wt:  weight of this event (passed in)
c---      wt2:  weight of this event (passed in)
c---     xmin:  lowest value to bin
c---     xmax:  highest value to bin
c---       dx:  bin width
c---   llplot:  equal to "lin"/"log" for linear/log scale
           
      call bookplot(n,tag,'m34',m34,wt,wt2,0._dp,8000._dp,100._dp,'lin')
      n=n+1
      call bookplot(n,tag,'pt3',pt3,wt,wt2,0._dp,4000._dp,50._dp,'lin')
      n=n+1
c-- for EW corrections, this is the histogram without them
      if (kewcorr /= knone) then
        call bookplot(n,tag,'m34 - no EW',m34,wt_noew,wt_noew**2,
     &   0._dp,8000._dp,100._dp,'lin')
        n=n+1
        wt_ew=wt_noew+wt
        call bookplot(n,tag,'m34 - with EW',m34,wt_ew,wt_ew**2,
     &   0._dp,8000._dp,100._dp,'lin')
        n=n+1
        call bookplot(n,tag,'m34 - +RELEW+',m34,wt_ew,wt_ew**2,
     &   0._dp,8000._dp,100._dp,'lin')
        n=n+1
        call bookplot(n,tag,'pt3 - no EW',pt3,wt_noew,wt_noew**2,
     &   0._dp,4000._dp,50._dp,'lin')
        n=n+1
        call bookplot(n,tag,'pt3 - with EW',pt3,wt_ew,wt_ew**2,
     &   0._dp,4000._dp,50._dp,'lin')
        n=n+1
        call bookplot(n,tag,'pt3 - +RELEW+',pt3,wt_ew,wt_ew**2,
     &   0._dp,4000._dp,50._dp,'lin')
        n=n+1
      else
        wt_ew=wt
      endif

      call bookplot(n,tag,'y3',y3,wt,wt2,-6._dp,6._dp,0.2_dp,'lin')
      n=n+1
      call bookplot(n,tag,'y4',y4,wt,wt2,-6._dp,6._dp,0.2_dp,'lin')
      n=n+1
      call bookplot(n,tag,'y34',y34,wt,wt2,-6._dp,6._dp,0.2_dp,'lin')
      n=n+1
      call bookplot(n,tag,'pt3',pt3,wt,wt2,0._dp,80._dp,2._dp,'lin')
      n=n+1
      call bookplot(n,tag,'pt4',pt4,wt,wt2,0._dp,80._dp,2._dp,'lin')
      n=n+1
      call bookplot(n,tag,'pt34',pt34,wt,wt2,0._dp,200._dp,2._dp,'lin')
      n=n+1
      call bookplot(n, tag,'m34',m34,wt,wt2,70._dp,110._dp,0.5_dp,'lin')
      n=n+1
      call bookplot(n,tag,'DeltaR35',r35,wt,wt2,0._dp,5._dp,0.1_dp,'lin')
      n=n+1
      call bookplot(n,tag,'y5',y5,wt,wt2,-3.2_dp,3.2_dp,0.5_dp,'lin')
      n=n+1
      call bookplot(n,tag,'pt5',pt5,wt,wt2,0._dp,100._dp,2._dp,'lin')
      n=n+1
      
c--- compute lepton asymmetry as a function of m34  
c--- (see for example Eq.(3) of PLB718 (2013) 752)
      p3(:)=p(3,:)
      p4(:)=p(4,:)
      p34(:)=p(3,:)+p(4,:)
      costheta=p34(3)/abs(p34(3))
     & *((p3(4)+p3(3))*(p4(4)-p4(3))-(p3(4)-p3(3))*(p4(4)+p4(3)))
     & /m34/sqrt(m34**2+p34(1)**2+p34(2)**2)
c--- these histograms must be kept
      if ((costheta > 0._dp) .or. (tag == tagbook)) then
        call bookplot(n, tag,'m34 forward lepton',
     &   m34,wt_ew,wt_ew**2,40._dp,200._dp,5._dp,'lin')
      endif
      n=n+1
      if ((costheta <= 0._dp) .or. (tag == tagbook)) then
        call bookplot(n, tag,'m34 backward lepton',
     &   m34,wt_ew,wt_ew**2,40._dp,200._dp,5._dp,'lin')
      endif
      n=n+1
c--- placeholder for lepton FB asymmetry (will be filled properly in histofin)
      call bookplot(n, tag,'lepton +FB+ asymmetry',
     & m34,wt_ew,wt_ew**2,40._dp,200._dp,5._dp,'lin')
      n=n+1

c--- these histograms must be kept
      if ((costheta > 0._dp) .or. (tag == tagbook)) then
        call bookplot(n, tag,'m34 forward lepton',
     &   m34,wt_ew,wt_ew**2,200._dp,8000._dp,200._dp,'lin')
      endif
      n=n+1
      if ((costheta <= 0._dp) .or. (tag == tagbook)) then
        call bookplot(n, tag,'m34 backward lepton',
     &   m34,wt_ew,wt_ew**2,200._dp,8000._dp,200._dp,'lin')
      endif
      n=n+1
c--- placeholder for lepton FB asymmetry (will be filled properly in histofin)
      call bookplot(n, tag,'lepton +FB+ asymmetry',
     & m34,wt_ew,wt_ew**2,200._dp,8000._dp,200._dp,'lin')
      n=n+1

      
! New histogram implementation
! note that the total number of array entries that are used to describe the
! bin boundaries should be 60 (histbinmax in histinclude.f)
! Note that putting 1/s in the title automatically normalizes by the
! computed total cross-section (may not be the same as the sum of the bins)
      nnew=1
      call dohist(nnew,'Z pt new',pt34,wt,wt2,
     & (/0._dp, 50._dp, 100._dp, 150._dp, 200._dp, 55*0._dp /) )
      nnew=nnew+1
      call dohist(nnew,'Z phi_eta^star new',phietastar,wt,wt2,
     & (/0._dp, 0.1_dp, 0.2_dp, 0.3_dp, 0.4_dp, 0.5_dp, 0.6_dp, 0.7_dp, 52*0._dp /) )
      nnew=nnew+1
      call dohist(nnew,'1/s ptll ATLAS1512.02192',pt34,wt,wt2,
     & (/ 0._dp,   2._dp,   4._dp,   6._dp,   8._dp,  10._dp,  12._dp,  14._dp, 
     &   16._dp,  18._dp,  20._dp, 22.5_dp,  25._dp, 27.5_dp,  30._dp,  33._dp,
     &   36._dp,  39._dp,  42._dp,  45._dp,  48._dp,  51._dp,  54._dp,  57._dp,
     &   61._dp,  65._dp,  70._dp,  75._dp,  80._dp,  85._dp,  95._dp, 105._dp,
     &  125._dp, 150._dp, 175._dp, 200._dp, 250._dp, 300._dp, 350._dp, 400._dp,
     &  470._dp, 550._dp, 650._dp, 900._dp, 16*0._dp/) )
      nnew=nnew+1      
      call dohist(nnew,'1/s phi ATLAS1512.02192',phietastar,wt,wt2,
     & (/0._dp   ,0.004_dp,0.008_dp,0.012_dp, 0.016_dp, 0.020_dp, 0.024_dp, 0.029_dp, 0.034_dp,
     &   0.039_dp,0.045_dp,0.051_dp,0.057_dp, 0.064_dp, 0.072_dp, 0.081_dp, 0.091_dp, 0.102_dp,
     &   0.114_dp,0.128_dp,0.145_dp,0.165_dp, 0.189_dp, 0.219_dp, 0.258_dp, 0.312_dp, 0.391_dp,
     &   0.524_dp,0.695_dp,0.918_dp,1.153_dp, 1.496_dp, 1.947_dp, 2.522_dp, 3.277_dp, 5.000_dp,
     &  10.000_dp, 23*0._dp /) )
      nnew=nnew+1

C--- Calculate angular functions
      sgn = abs(p34(3))/p34(3)
      p1(1) = 0
      p1(2) = 0
      p1(3) = sgn*sqrts/2.0
      p1(4) = sqrts/2.0
      p2(1) = 0
      p2(2) = 0
      p2(3) = -sgn*sqrts/2.0
      p2(4) = sqrts/2.0

      call boost2(m34,p34,p1,p1)
      call boost2(m34,p34,p2,p2)
      call boost2(m34,p34,p3,boostedE)
      call boost2(m34,p34,p4,boostedP)
      call boost2(m34,p34,p34,boostedZ)

      call unitVec(p1(1:3),p1(1:3))
      call unitVec(p2(1:3),p2(1:3))
      tmp(1) = p1(1) - p2(1)
      tmp(2) = p1(2) - p2(2)
      tmp(3) = p1(3) - p2(3)
      call unitVec(tmp,CSAxis)

      call cross3(p1(1:3),p2(1:3),yAxis)
      call unitVec(yAxis,yAxis)
      call cross3(yAxis,CSAxis,xAxis)
      call unitVec(xAxis,xAxis)
      
      costheta = cos(angle(boostedE(1:3),CSAxis))
      sintheta = 1.0-costheta**2
      phi = atan2(dot3(boostedE(1:3),yAxis),dot3(boostedE(1:3),xAxis))
      if(phi < 0) phi = phi + 2*pi

      A0 = (10.0/3.0*(1.0-3.0*costheta**2)+2.0/3.0)*wt 
      call dohist(nnew,'A0',pt34,A0,A0**2,
     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
      nnew=nnew+1
      A1 = 5*sintheta*costheta*cos(phi)*wt
      call dohist(nnew,'A1',pt34,A1,A1**2,
     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
      nnew=nnew+1
      A2 = 10*sintheta**2*cos(2*phi)*wt
      call dohist(nnew,'A2',pt34,A2,A2**2,
     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
      nnew=nnew+1
      A3 = 4*sintheta*cos(phi)*wt
      call dohist(nnew,'A3',pt34,A3,A3**2,
     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
      nnew=nnew+1
      A4 = 4*costheta*wt
      call dohist(nnew,'A4',pt34,A4,A4**2,
     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
      nnew=nnew+1
!      A5 = 5*sintheta*sin(2*phi)*wt
!      call dohist(nnew,'A5',pt34,A5,A5**2,
!     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
!     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
!     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
!      nnew=nnew+1
!      A6 = 5*costheta*sintheta*sin(phi)*wt
!      call dohist(nnew,'A6',pt34,A6,A6**2,
!     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
!     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
!     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
!      nnew=nnew+1
!      A7 = 4*sintheta**2*cos(phi)*sin(phi)*wt
!      call dohist(nnew,'A7',pt34,A7,A7**2)
!     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
!     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
!     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
!      nnew=nnew+1
      call dohist(nnew,'pt342',pt34,wt,wt2,
     &(/ 0._dp, 2.5_dp  ,5._dp,  8._dp,11.4_dp,14.9_dp,18.5_dp, 22._dp,25.5_dp,
     &  29._dp,36.4_dp,40.4_dp,44.9_dp,50.2_dp,56.4_dp,63.9_dp,73.4_dp,85.4_dp,
     & 105._dp,132._dp,173._dp,253._dp,600._dp, 28*0._dp /) )
      nnew=nnew+1



************************************************************************
*                                                                      *
*     FINAL BOOKKEEPING                                                *
*                                                                      *
************************************************************************

c--- We have over-counted the number of histograms by 1 at this point
      n=n-1

c--- Ensure the built-in maximum number of histograms is not exceeded    
      call checkmaxhisto(n)

c--- Set the maximum number of plots, on the first call
      if (first) then
        first=.false.
        nplotmax=n
      endif
      
      return 
      end
      
