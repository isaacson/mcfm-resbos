program test
    use libresbos
    implicit none
    type(resbos) :: resbosvar
    double precision, allocatable :: x(:)
    double precision :: y

    allocate(x(5))
    x(1) = 91.18
    x(2) = 10
    x(3) = 0
    x(4) = 0
    x(5) = 0

    ! Create an object of type resbos
    resbosvar = resbos('resbos.config')

    ! Call bound procedures
    y = resbosvar%mesq(x)
    write(*,*) y
#ifdef __GNUC__
!    call resbosvar%delete
#endif
end program
