! Main routine to be called: switches to initialization or filling as appropriate
      subroutine dohist(n,titlex,var,wt,wt2,xleftin)
      implicit none
      include 'types.f'
      include 'histinclude.f'
      include 'kpart.f'
      integer n
      character*(*) titlex
      character*20 trimtitle
      real(dp) :: var,wt,wt2,xleftin(histbinmax)
      logical, save :: threadfirst(histmax)=.true.
!$omp threadprivate(threadfirst)
      
!      write(6,*) 'entered dohist: n, threadfirst(n)',n, threadfirst(n)
      if (threadfirst(n)) then
        trimtitle = trim(titlex)
        call histbook(n,trimtitle,xleftin)
        threadfirst(n)=.false.
      else
         if ((kpart==klord).or. (kpart==kvirt).or. (kpart==kfrag)
     &  .or. (kpart==ksnlo).or. (kpart==knnlo).or. (kpart==kresumnll)
     &  .or. (kpart==kresumnnll)) then
           call histfill(n,var,wt,wt2)
         else
           call realhistfill(n,var,wt)
         endif
      endif
      
      return
      end
      
      
! Histogram initialization
      subroutine histbook(n,trimtitle,xleftin)
      implicit none
      include 'types.f'
      include 'histinclude.f'
      integer n,k
      character*20 trimtitle
      real(dp) :: xleftin(histbinmax)
      real(dp), parameter :: tiny = 1.e-8_dp

      if (n > histmax) then
        write(6,*) ' Too many histograms filled in dohist!'
        write(6,*) n,' > ',histmax,' allowed!'
        stop
      endif
      
      if (enthist(n) > -1) return ! nothing to do, already set up
      
      titlehist(n)=trimtitle
      
      do k=1,histbinmax-1
        xhistleft(n,k)=xleftin(k)
        dxhist(n,k)=xleftin(k+1)-xleftin(k)
        if (dxhist(n,k) < tiny) then
          finalbin(n)=k-1
          exit
        endif
      enddo
      
      enthist(n)=0
      lohist(n)=0
      hihist(n)=0
      entbin(n,:)=0
      
      yhist(n,:)=0._dp
      ysqhist(n,:)=0._dp

!      write(6,*) 'initialized histogram',n,titlehist(n)
!      write(6,*) 'with no. of bins = ',finalbin(n)
!      write(6,*) 'bins initialized:'
!      do k=1,finalbin(n)
!        write(6,*) xhistleft(n,k),dxhist(n,k)
!      enddo

      return
      end


! Histogram filling
      subroutine histfill(n,var,wt,wt2)
      implicit none
      include 'types.f'
      include 'histinclude.f'
      integer n,k,kbin
      character*20 trimtitle
      real(dp) :: var,wt,wt2
      real(dp), parameter :: tiny = 1.e-8_dp
      
      kbin=0
      do k=1,finalbin(n)
        if ((var >= xhistleft(n,k)) .and. (var <xhistleft(n,k)+dxhist(n,k))) then
          kbin=k
          exit
        endif
      enddo

      if (kbin > 0) then
!        write(6,*) 'binning histogram ',n
!        write(6,*) 'variable, bin',var,kbin
!$omp atomic
        enthist(n)=enthist(n)+1
!$omp atomic
        entbin(n,kbin)=entbin(n,kbin)+1
!$omp atomic
        yhist(n,kbin)=yhist(n,kbin)+wt
!$omp atomic
        ysqhist(n,kbin)=ysqhist(n,kbin)+wt2
      else
        if (var < xhistleft(n,1)) then
!$omp atomic
          lohist(n)=lohist(n)+1
        else
!$omp atomic
          hihist(n)=hihist(n)+1
        endif
      endif
        
      return
      end
      

! Initialization for temporary real histograms
c--- zero out all entries in the temporary histograms used for
c--- binning the weights in the real contribution      
      subroutine realhistzero
      implicit none
      include 'types.f'
      include 'histinclude.f'
            
      histrealibin(:,:)=0             ! which bin entry should be filled
      realyhist(:,:)=0._dp            ! ... with this weight
      histrealicont(:)=0              ! counter for number of entries in ibin
      
      return
      end


! Fill bin entries in real contribution
      subroutine realhistfill(n,var,wt)
      implicit none
      include 'types.f'
      include 'histinclude.f'
      integer:: n,k,kbin
      real(dp):: var,wt
      
      kbin=0
      do k=1,finalbin(n)
        if ((var >= xhistleft(n,k)) .and. (var <xhistleft(n,k)+dxhist(n,k))) then
          kbin=k
          exit
        endif
      enddo
      
      if (kbin > 0) then
        histrealicont(n)=histrealicont(n)+1       ! increase counter by one
        histrealibin(histrealicont(n),n)=kbin     ! record bin number for weight
        realyhist(histrealicont(n),n)=wt          ! record actual weight
      endif

      return
      end


! Add temporary real histograms to the cumulative ones
      subroutine realhistadd(wgt)
      implicit none
      include 'types.f'
      include 'histinclude.f'
      integer n,i1,i2
      real(dp):: wgt,tmpvar

      do n=1,histmax
      
      if (enthist(n) == -1) cycle         ! skip histogram if  not initialized
      
      if (histrealicont(n) == 0) cycle    ! skip histogram if no new entries
!$omp atomic
        enthist(n)=enthist(n)+1
        do i1=1,histrealicont(n)
          if (histrealibin(i1,n) == 0) cycle     ! skip if already counted
c---     look for other entries in the same bin before adding
          do i2=i1+1,histrealicont(n)
          if (histrealibin(i2,n) == histrealibin(i1,n)) then
            histrealibin(i2,n)=0
            realyhist(i1,n)=realyhist(i1,n)+realyhist(i2,n)
          endif
          enddo
          tmpvar=realyhist(i1,n)**2
          if (realyhist(i1,n) .ne. 0._dp) then
!$omp atomic
            entbin(n,histrealibin(i1,n))=entbin(n,histrealibin(i1,n))+1
          endif
!$omp atomic
          yhist(n,histrealibin(i1,n))=yhist(n,histrealibin(i1,n))+realyhist(i1,n)
!$omp atomic
          ysqhist(n,histrealibin(i1,n))=ysqhist(n,histrealibin(i1,n))+tmpvar
        enddo
      enddo
      
      return
      end
      

! This routine is called at the end of every VEGAS integration shot
! to compute and update histogram errors
      subroutine histerrorupdate()
      implicit none
      include 'types.f'
      include 'mpif.h'
      include 'constants.f'
      include 'histinclude.f'
      include 'mpicommon.f'
      integer ierr,n,k
      integer(kind=8) :: redihist(histmax),redihist2(histmax,histbinmax)
      real(dp) :: redhist(histmax, histbinmax),errsq,M
      
! First reduce all MPI arrays
      call mpi_reduce(yhist,redhist,histmax*histbinmax,mpi_double_precision,mpi_sum,0,mpi_comm_world,ierr)
      yhist(:,:)=redhist(:,:)
      call mpi_reduce(ysqhist,redhist,histmax*histbinmax,mpi_double_precision,mpi_sum,0,mpi_comm_world,ierr)
      ysqhist(:,:)=redhist(:,:)
      call mpi_reduce(enthist,redihist,histmax,mpi_integer8,mpi_sum,0,mpi_comm_world,ierr)
      enthist(:)=redihist(:)
      call mpi_reduce(lohist,redihist,histmax,mpi_integer8,mpi_sum,0,mpi_comm_world,ierr)
      lohist(:)=redihist(:)
      call mpi_reduce(hihist,redihist,histmax,mpi_integer8,mpi_sum,0,mpi_comm_world,ierr)
      hihist(:)=redihist(:)
      call mpi_reduce(entbin,redihist2,histmax*histbinmax,mpi_integer8,mpi_sum,0,mpi_comm_world,ierr)
      entbin(:,:)=redihist2(:,:)
      
      if (rank == 0) then

        do n=1,histmax
          if (enthist(n) > 0) then
! update counters
            partenthist(n)=partenthist(n)+enthist(n)
            partlohist(n)=partlohist(n)+lohist(n)
            parthihist(n)=parthihist(n)+hihist(n)
! compute errors for current iteration and update histogram entries
            do k=1,finalbin(n)
              M=real(entbin(n,k),dp)
              errsq=abs(ysqhist(n,k)-yhist(n,k)**2/M)*(M/(M-1._dp))
              partentbin(n,k)=partentbin(n,k)+entbin(n,k)
              if (errsq > zero) then
                partyhist(n,k)=partyhist(n,k)+yhist(n,k)/errsq
                partehist(n,k)=partehist(n,k)+one/errsq
              endif
            enddo
          endif
        enddo

      endif

! zero all entries for current iteration
      enthist(:)=0
      lohist(:)=0
      hihist(:)=0
      entbin(:,:)=0
      yhist(:,:)=zero
      ysqhist(:,:)=zero

      return
      end



! This routine is called at the end of every VEGAS sweep to update the total
! histograms with the result from each part
      subroutine tothistupdate()
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'histinclude.f'
      include 'mpicommon.f'
      integer n,k
      
      if (rank == 0) then
      
        do n=1,histmax
          totenthist(n)=totenthist(n)+partenthist(n)
          totlohist(n)=totlohist(n)+partlohist(n)
          tothihist(n)=tothihist(n)+parthihist(n)
          do k=1,finalbin(n)
            if (partehist(n,k) > zero) then
              partyhist(n,k)=partyhist(n,k)/partehist(n,k)
              partehist(n,k)=one/sqrt(partehist(n,k))
            endif
            totyhist(n,k)=totyhist(n,k)+partyhist(n,k)
            totehist(n,k)=sqrt(totehist(n,k)**2+partehist(n,k)**2)
            totentbin(n,k)=totentbin(n,k)+partentbin(n,k)
          enddo
        enddo

      endif
      
! reset individual part histograms to zero
      partenthist(:)=0
      partlohist(:)=0
      parthihist(:)=0
      partentbin(:,:)=0
      partyhist(:,:)=zero
      partehist(:,:)=zero
      
      return
      end



! Histogram printing
      subroutine histprint(n,sig)
      implicit none
      include 'types.f'
      include 'histinclude.f'
      integer k,n
      logical writenorm
      real(dp) :: sig,err,y
      character*20 title
      
      if (titlehist(n)(1:4) == '1/s ') then
        writenorm=.true.
        title=titlehist(n)(5:)
      else
        writenorm=.false.
        title=titlehist(n)
      endif
      
      write(6,*)
      write(6,*) 'histprint: n,titlehist(n)',n,title
      write(6,*) 'totenthist(n),totlohist(n),tothihist(n)',totenthist(n),totlohist(n),tothihist(n)
      
      do k=1,finalbin(n)
        y=totyhist(n,k)
        err=totehist(n,k)
! normalize by bin width if desired
        y=y/dxhist(n,k)
        err=err/dxhist(n,k)
        write(6,'(3X,2G13.6,2(2X,G13.4))') xhistleft(n,k),xhistleft(n,k)+dxhist(n,k),y,err
      enddo
      
      if (writenorm) then
        write(6,*)
        write(6,*) 'histprint: n,titlehist(n)',n,titlehist(n)
        write(6,*) 'totenthist(n),totlohist(n),tothihist(n)',totenthist(n),totlohist(n),tothihist(n)
      
        do k=1,finalbin(n)
          y=totyhist(n,k)
          err=totehist(n,k)
          y=y/sig
          err=err/sig
          write(6,'(3X,2G13.6,2(2X,G13.4))') xhistleft(n,k),xhistleft(n,k)+dxhist(n,k),y,err
        enddo
      endif
      
      return
      end


! Histogram Topdrawer output
      subroutine histtop(n,sig)
      implicit none
      include 'types.f'
      include 'histinclude.f'
      integer k,n
      logical writenorm
      real(dp) :: sig,err,y,yint,yerr
      character*20 title
      
      if (titlehist(n)(1:4) == '1/s ') then
        writenorm=.true.
        title=titlehist(n)(5:)
      else
        writenorm=.false.
        title=titlehist(n)
      endif
      
      write(99,101) trim(title),trim(title),trim(title),
     & 'lin',xhistleft(n,1),xhistleft(n,finalbin(n))+dxhist(n,finalbin(n))

      yint=0._dp
      yerr=0._dp
      do k=1,finalbin(n)
        y=totyhist(n,k)
        err=totehist(n,k)
        yint=yint+y
        yerr=yerr+err**2
! normalize by bin width if desired
        y=y/dxhist(n,k)
        err=err/dxhist(n,k)
        write(99,'(3X,2G13.6,2(2X,G13.4))') xhistleft(n,k)+dxhist(n,k)/2._dp,dxhist(n,k)/2._dp,y,err
      enddo
      yerr=sqrt(yerr)
      
      write(99,*) 'PLOT'
      write(99,300) yint,yerr,totenthist(n),totlohist(n),tothihist(n)
      write(99,*) '   NEW PLOT'

      if (writenorm) then
        write(99,101) trim(titlehist(n)),trim(titlehist(n)),trim(titlehist(n)),
     &   'lin',xhistleft(n,1),xhistleft(n,finalbin(n))+dxhist(n,finalbin(n))

        yint=0._dp
        yerr=0._dp
        do k=1,finalbin(n)
          y=totyhist(n,k)
          err=totehist(n,k)
          y=y/sig
          err=err/sig
          yint=yint+y
          yerr=yerr+err**2
          write(99,'(3X,2G13.6,2(2X,G13.4))') xhistleft(n,k)+dxhist(n,k)/2._dp,dxhist(n,k)/2._dp,y,err
        enddo
        yerr=sqrt(yerr)
      
        write(99,*) 'PLOT'
        write(99,300) yint,yerr,totenthist(n),totlohist(n),tothihist(n)
        write(99,*) '   NEW PLOT'
      endif

      return

  101 format( /1x,
     &' SET WINDOW Y 2.5 TO 7.'/,1X,
     &' SET WINDOW X 2.5 TO 10.'/,1X,
     &' SET SYMBOL 5O SIZE 1.8'/,1X,
     &' TITLE TOP SIZE=3','"',A,'"',/1X,
     &' TITLE BOTTOM ','"',A,'"',/1X,
     &' TITLE LEFT ','"dS/d',A,' [fb]"',/1X,
     &' CASE       ','" G"',/1X,
     &' SET SCALE Y ',A5,/1X,
     &' (SET TICKS TOP OFF)   '/1x,     
     &' SET LIMITS X ',F12.5,' ',F12.5,/1X,
     &' SET ORDER X DX Y DY')
  300 format( /1x,                               
     &' BOX 6.9 1.0 SIZE 8.5 0.8'/,1X,
     &' SET WINDOW Y 0. TO 2.'/,1X,
     &' SET TITLE SIZE -1.5'/1X,
     &' TITLE 2.8 1.2 "INTEGRAL =',E12.5,'       ERROR =',E12.5,'"',/1X,
     &' TITLE 2.8 0.8 "Entries =',I9,2x,'U`flow =',I9,2X
     &                                 ,'O`flow =',I9,'"',/1X,
     &' SET TITLE SIZE -2')

      end




