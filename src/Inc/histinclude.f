! main histogram definitions
      integer, parameter :: histmax = 12, histbinmax= 60
      integer(kind=8) :: enthist(histmax), lohist(histmax), hihist(histmax),
     & finalbin(histmax),entbin(histmax, histbinmax)
      real(dp) :: xhistleft(histmax,histbinmax),dxhist(histmax,histbinmax),
     & yhist(histmax, histbinmax),ysqhist(histmax, histbinmax)
      character*20 titlehist(histmax)
      
      common/inthist/ enthist, lohist, hihist, finalbin, entbin
      common/dphist/ xhistleft, dxhist, yhist, ysqhist
      common/charhist/ titlehist

! definitions for real contributions
      include 'maxd.f'
      integer(kind=8):: histrealibin(maxd+1,histmax),histrealicont(histmax)
      real(dp):: realyhist(maxd+1,histmax)
      common/realhist/histrealibin,histrealicont,realyhist
!$omp threadprivate(/realhist/)

! definitions for histograms for an individual contribution
      integer(kind=8) :: partenthist(histmax), partlohist(histmax), parthihist(histmax),
     & partentbin(histmax, histbinmax)
      real(dp) :: partyhist(histmax, histbinmax),partehist(histmax, histbinmax)

      common/partinthist/ partenthist, partlohist, parthihist, partentbin
      common/partdphist/ partyhist, partehist

! definitions for histograms for the total
      integer(kind=8) :: totenthist(histmax), totlohist(histmax), tothihist(histmax),
     & totentbin(histmax, histbinmax)
      real(dp) :: totyhist(histmax, histbinmax),totehist(histmax, histbinmax)

      common/totinthist/ totenthist, totlohist, tothihist, totentbin
      common/totdphist/ totyhist, totehist

