      function resumint(r,wgt)
      use resbosmod
      implicit none
      include 'types.f'
      real(dp):: resumint
      include 'constants.f'
      include 'nf.f'
      include 'mxpart.f'
      include 'masses.f'
      include 'limits.f'
      include 'npart.f'
      include 'debug.f'
      include 'new_pspace.f'
      include 'vegas_common.f'
      include 'sprods_com.f'
      include 'scale.f'
      include 'facscale.f'
      include 'dynamicscale.f'
      include 'noglue.f'
      include 'kprocess.f'
      include 'efficiency.f'
      include 'maxwt.f'
      include 'phasemin.f'
      include 'PDFerrors.f'
      include 'wts_bypart.f'
      include 'stopscales.f'
      include 'frag.f'
      include 'ipsgen.f'
      include 'outputoptions.f'
      include 'outputflags.f'
      include 'runstring.f'
      include 'energy.f'
      include 'VVstrong.f'
      include 'dm_params.f'
      include 'initialscales.f'
      include 'toploopgaga.f'
      include 'scalevar.f'
      include 'qcdcouple.f'
      include 'couple.f'
      include 'nlooprun.f'
      include 'badpoint.f'
      include 'ewcorr.f'
c---- SSbegin
      include 'reweight.f'
c---- SSend
c --- DSW. To store flavour information :
      include 'nflav.f'
c --- DSW.
      include 'x1x2.f'
      include 'bypart.f'
      include 'taucut.f' ! for usescet
      integer:: pflav,pbarflav
c--- To use VEGAS random number sequence :
      real(dp):: ran2
      integer:: ih1,ih2,j,k,nvec,sgnj,sgnk,ii,i1,i2,i3,i4
      integer:: i,t
      integer:: itrial
      real(dp):: alphas,msqtrial,xmsqvar(2),
     & fx1up(-nf:nf),fx2up(-nf:nf),fx1dn(-nf:nf),fx2dn(-nf:nf)
      real(dp):: r(mxdim),W,xmsq,val,val2,ptmp,
     & fx1(-nf:nf),fx2(-nf:nf),p(mxpart,4),pjet(mxpart,4),plo(mxpart,4),
     & pswt,rscalestart,fscalestart,
     & fx1_H(-nf:nf),fx2_H(-nf:nf),fx1_L(-nf:nf),fx2_L(-nf:nf),
     & fxb1(-nf:nf),fxb2(-nf:nf),xmsq_array(-nf:nf,-nf:nf)
      real(dp):: wgt,msq(-nf:nf,-nf:nf),m3,m4,m5,xmsqjk,msq_real(-nf:nf,-nf:nf)
      real(dp):: xmsqjk_noew,msq_noew(-nf:nf,-nf:nf),xmsq_noew,resumint_noew
      real(dp):: msq1(-nf:nf,-nf:nf),
     & msq4(-nf:nf,-nf:nf,-nf:nf,-nf:nf),pswtdip
      real(dp):: flux,vol,vol_mass,vol3_mass,vol_wt,BrnRat
      real(dp):: xmsq_bypart(-1:1,-1:1)
      logical:: bin,includedipole,checkpiDpjk
      real(dp):: b1scale,q2scale,q1scale,b2scale,Q,qt,y, dot, pttwo,yraptwo
      real(dp):: twomass
      real(dp) :: lplus, lminus, pplus, pminus, costheta, theta, resbos_result
      external qg_tbq,BSYqqb_QQbdk_gvec,qqb_QQbdk,qg_tbqdk,qg_tbqdk_gvec,
     & qqb_Waa,qqb_Waa_mad
     & qqb_Zbbmas,qqb_Zbbmas,qqb_totttZ,qqb_totttZ_mad
      common/density/ih1,ih2
      common/bin/bin
      common/BrnRat/BrnRat
      common/bqscale/b1scale,q2scale,q1scale,b2scale
      external qq_tchan_ztq,qq_tchan_ztq_mad
      external qq_tchan_htq,qq_tchan_htq_mad,qq_tchan_htq_amp
      external qqb_gamgam_g,qqb_gmgmjt_gvec,gg_hzgamg,gg_hg_zgam_gvec

      real(dp):: xjac, T1, U, Tkin, M_T, x1Min
!$omp threadprivate(/bqscale/)

!$omp atomic
      ntotshot=ntotshot+1
      resumint=0._dp
c--- ensure isolation code does not think this is fragmentation piece
      z_frag=0._dp
      
      W=sqrts**2
      p(:,:)=0._dp
      pjet(:,:)=0._dp

c---- set default reweight to 1 (hence no reweighting)
      reweight = 1.0_dp

      if (new_pspace) then
        call gen_lops(r,plo,pswt,*999)
!        call writeout(plo)
        npart=npart+1
        call multichan(r(ndim-2),r(ndim-1),r(ndim),
     &                 r(ndim+2),plo,p,pswtdip,*999)

        pswt=pswt*pswtdip
      else
        call gen_resumps(r,p,pswt,*999)
C        call gen_realps(r,p,pswt,*999)
      endif

      nvec=npart+2
      call dotem(nvec,p,s)

c--- (moved to includedipole) impose cuts on final state
c      if (kcase.ne.kvlchk6 .and. kcase.ne.ktautau) then
c        call masscuts(p,*999)
c      endif

      if (usescet) then
c----reject event if any tau is too small
        call smalltau(p,npart,*999)
      else
c---- reject event if any s(i,j) is too small
       call smalls(s,npart,*999)
      endif
c--- see whether this point will pass cuts - if it will not, do not
c--- bother calculating the matrix elements for it, instead bail out
      if (includedipole(0,p) .eqv. .false.) then
        goto 999
      endif

c      call writeout(p)
c      stop
      if (dynamicscale) call scaleset(initscale,initfacscale,p)
      
      xx(1)=-2._dp*p(1,4)/sqrts
      xx(2)=-2._dp*p(2,4)/sqrts

c      if (debug) write(*,*) 'Reconstructed x1,x2 ',xx(1),xx(2)

      if ((doscalevar) .and. (foundpow .eqv. .false.)) then
        itrial=1
      endif
   66 continue
    
      Q = sqrt(2.0*dot(p,3,4))
      qt = pttwo(3,4,p)
      y = yraptwo(3,4,p)
      Lplus = p(3,4)+p(3,3)
      Lminus = p(3,4)-p(3,3)
      Pplus = p(4,4)+p(4,3)
      Pminus = p(4,4)-p(4,3)

      costheta = (Lplus*Pminus - Lminus*Pplus)/(Q*sqrt(Q*Q+qt*qt))
      theta = acos(costheta)

c--- Calculate the required matrix elements      
      if     (kcase==kW_only) then
        call qqb_w(p,msq)
      elseif (kcase==kWgamma) then
        call qqb_wgam(p,msq)
      elseif (kcase==kZ_only) then
        resbos_result = resbosvar%mesq(Q,qt,y,theta)
        if (qt > 2) then
            call qqb_z1jet(p,msq_real)
        endif
      elseif (kcase==kZgamma) then
        call set_anomcoup(p)
        call qqb_zgam_new(p,msq)
      elseif (kcase==kWWqqbr) then
        call qqb_ww(p,msq)
      elseif (kcase==kWZbbar) then
        call qqb_wz(p,msq)
      elseif (kcase==kZZlept) then
        call qqb_zz(p,msq)
      elseif (kcase==kggfus0) then
        call gg_h(p,msq)
      else
        write(6,*) 'Unimplemented process in resumint : kcase=',kcase
        stop 
      endif

      currentPDF=0

c--- do not calculate the flux if we're only checking the volume      
c      if (case(1:4) .ne. 'vlch') then      
      flux=fbGeV2/(2._dp*xx(1)*xx(2)*W)
c      endif

      xmsq = 0.0
      if(qt > 2) then
        call fdist(ih1,xx(1),facscale,fx1)
        call fdist(ih2,xx(2),facscale,fx2)

        do j=-nflav,nflav
        do k=-nflav,nflav
         xmsqjk = fx1(j)*fx2(k)*msq_real(j,k)
         xmsq=xmsq+xmsqjk
        enddo
        enddo
      endif

C      resumint=flux*pswt*xmsq/BrnRat
C      if(resbos_result > 0 .and. qt < Q) then
C          print*, Q, qt, y, resbos_result*2.0*Q**4/xjac
C      endif
      M_T = sqrt(qt**2+Q**2)
      Tkin = -M_T*sqrts*exp(-y)+Q**2
      U = -M_T*sqrts*exp(y)+Q**2
      x1Min = -U/(sqrts**2+Tkin-Q**2)
      T1 = abs(xx(1)*sqrts**2+U-Q**2)
      xjac = (1-x1Min)
      
      resbos_result = 1E3*resbos_result ! Convert from pb to fb
      resbos_result = resbos_result*512.0*pi**4*T1/xjac ! Remove real ps factors
      resbos_result = resbos_result*pi/2.0/(2.0*pi)**3/8.0/(pi*qt)/(2*Q**2) !add in resbos factors
      resumint = (resbos_result+xmsq*flux)*pswt/BrnRat
C      resumint = (xmsq*flux)*pswt/BrnRat
c      resumint = (resbos_result)*pswt/BrnRat
      
      call getptildejet(0,pjet)
      
      call dotem(nvec,pjet,s)

      val=resumint*wgt
      val2=val**2
      if(val.ne.val) then
!         write(6,*) 'resumint val = ',val
!         write(6,*) 'Discarding point with random variables',r
         resumint=zip
         val=zip
         goto 999
      endif
c---  SSbegin
      resumint = resumint*reweight
c---  SSend
c--- update the maximum weight so far, if necessary
c---  but not if we are already unweighting ...
      if ((.not.unweight) .and. (abs(val) > wtmax)) then
!$omp critical(MaxWgt)
        wtmax=abs(val)
!$omp end critical(MaxWgt)
      endif

      if (bin) then
c--- for EW corrections, make additional weight available inside common block
        if (kewcorr /= knone) then
          wt_noew=resumint_noew*wgt
        endif
        call nplotter(pjet,val,val2,0)
c---  POWHEG-style output if requested
        if (writepwg) then
!$omp critical(pwhgplot)
          call pwhgplotter(p,pjet,val,0)
!$omp end critical(pwhgplot)
        endif
      endif

c --- Check weights :
      if (unweight) then
c       write(6,*) 'Test weight ',val,' against max ',wtmax
        wtabs = abs(val)
c--- note that r(ndim+2) is reserved for new_pspace in real, so unused at LO
        if (r(ndim+2) < (wtabs/wtmax)) then
c         write(6,*) 'Keep event with weight',val
          if (wtabs<wtmax) then
            newwt = 1._dp
          else
            newwt = wtabs/wtmax
          endif
          if (newwt > 1.0_dp) then
            write(6,*) 'WARNING : resumint : event with |weight| > 1.',
     &                 ' |weight| = ',newwt
          endif
c ---     just in case the weight was negative :
          newwt = newwt*sign(one,val)
c         call nplotter(pjet,newwt,newwt,0)
!$omp critical(resumintWriteLHE)
          call mcfm_writelhe(pjet,xmsq_array,newwt)
!$omp end critical(resumintWriteLHE)
        endif
      endif

      return

 999  continue
      resumint=0._dp
!$omp atomic
      ntotzero=ntotzero+1
      
      return
      end


