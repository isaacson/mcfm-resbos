      function resumnnllvint(r,wgt)
      use resbosmod
      implicit none
      include 'types.f'
      real(dp):: resumnnllvint
      include 'constants.f'
      include 'nf.f'
      include 'mxpart.f'
      include 'cplx.h'
      include 'masses.f'
      include 'limits.f'
      include 'npart.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'agq.f'
      include 'PR_new.f'
      include 'PR_cs_new.f'
      include 'PR_h2j.f'
      include 'PR_twojet.f'
      include 'PR_stop.f'
      include 'PR_mix.f'
      include 'msq_cs.f'
      include 'msq_struc.f'
      include 'msq_mix.f'
      include 'debug.f'
      include 'new_pspace.f'
      include 'vegas_common.f'
      include 'sprods_com.f'
      include 'scale.f'
      include 'facscale.f'
      include 'dynamicscale.f'
      include 'noglue.f'
      include 'kprocess.f'
      include 'efficiency.f'
      include 'lc.f'
      include 'heavyflav.f'
      include 'b0.f'
      include 'maxwt.f'
      include 'phasemin.f'
      include 'PDFerrors.f'
      include 'wts_bypart.f'
      include 'nores.f'
      include 'stopscales.f'
      include 'frag.f'
      include 'ipsgen.f'
      include 'outputoptions.f'
      include 'outputflags.f'
      include 'TRbadpoint.f'
      include 'runstring.f'
      include 'energy.f'
      include 'first.f'
      include 'VVstrong.f'
      include 'dm_params.f'
      include 'initialscales.f'
      include 'toploopgaga.f'
      include 'scalevar.f'
      include 'qcdcouple.f'
      include 'couple.f'
      include 'nlooprun.f'
      include 'badpoint.f'
      include 'ewcorr.f'
      include 'ewcouple.f'
      include 'flags.f'
      include 'ppmax.f'
      include 'kpart.f'
      include 'hbbparams.f'
      include 'mpicommon.f'
c---- SSbegin
      include 'reweight.f'
c---- SSend
c --- DSW. To store flavour information :
      include 'nflav.f'
c --- DSW.
      include 'x1x2.f'
      include 'bypart.f'
      include 'taucut.f' ! for usescet

      real(dp):: AP(-1:1,-1:1,3),APqg_mass,AP_mix(-1:1,-1:1,0:3,3)

      integer:: pflav,pbarflav
c--- To use VEGAS random number sequence :
      real(dp):: ran2
      integer:: ih1,ih2,j,k,nvec,sgnj,sgnk,ii,i1,i2,i3,i4
      integer:: cs, ia, ib, ic, is
      integer:: i,t
      integer:: itrial
      real(dp):: alphas,msqtrial,xmsqvar(2),
     & fx1up(-nf:nf),fx2up(-nf:nf),fx1dn(-nf:nf),fx2dn(-nf:nf)
      real(dp):: r(mxdim),W,xmsq,val,val2,ptmp,
     & fx1(-nf:nf),fx2(-nf:nf),p(mxpart,4),pjet(mxpart,4),plo(mxpart,4),
     & pswt,rscalestart,fscalestart,
     & fx1_H(-nf:nf),fx2_H(-nf:nf),fx1_L(-nf:nf),fx2_L(-nf:nf),
     & fxb1(-nf:nf),fxb2(-nf:nf),xmsq_array(-nf:nf,-nf:nf)
      real(dp)::wgt,msq(-nf:nf,-nf:nf),m3,m4,m5,xmsqjk,msq_real(-nf:nf,-nf:nf),
     & msqv(-nf:nf,-nf:nf),fx1z(-nf:nf),fx2z(-nf:nf),
     & msq_qq,msq_aa,msq_aq,msq_qa,msq_qg,msq_gq,epcorr,
     & msq0(-nf:nf,-nf:nf),msqm1(-nf:nf,-nf:nf),xlog,
     & bit2(-nf:nf,-nf:nf),bit1(-nf:nf,-nf:nf),bit0(-nf:nf,-nf:nf),xmsq_old
      real(dp):: xmsqjk_noew,msq_noew(-nf:nf,-nf:nf),xmsq_noew,resumnnllvint_noew
      real(dp):: msq1(-nf:nf,-nf:nf),
     & msq4(-nf:nf,-nf,-nf:nf,-nf:nf),pswtdip
      real(dp):: flux,vol,vol_mass,vol3_mass,vol_wt,BrnRat
      real(dp):: xmsq_bypart(-1:1,-1:1)
      logical:: bin,includedipole,checkpiDpjk
      real(dp):: b1scale,q2scale,q1scale,b2scale,QV,qt,y, dot, pttwo,yraptwo
      real(dp):: twomass
      real(dp) :: lplus, lminus, pplus, pminus, costheta, theta, resbos_result
      external qg_tbq,BSYqqb_QQbdk_gvec,qqb_QQbdk,qg_tbqdk,qg_tbqdk_gvec,
     & qqb_Waa,qqb_Waa_mad
     & qqb_Zbbmas,qqb_Zbbmas,qqb_totttZ,qqb_totttZ_mad
      common/density/ih1,ih2
      common/bin/bin
      common/BrnRat/BrnRat
      common/bqscale/b1scale,q2scale,q1scale,b2scale
      external qq_tchan_ztq,qq_tchan_ztq_mad
      external qq_tchan_htq,qq_tchan_htq_mad,qq_tchan_htq_amp
      external qqb_gamgam_g,qqb_gmgmjt_gvec,gg_hzgamg,gg_hg_zgam_gvec
      integer, save:: nshot=1
      integer:: rvcolourchoice
      common/rvcolourchoice/rvcolourchoice
      real(dp):: z,x1onz,x2onz,xjac2,omz

      real(dp):: xjac, T1, U, Tkin, M_T, x1Min
!$omp threadprivate(/rvcolourchoice/)
!$omp threadprivate(nshot)
!$omp threadprivate(/bqscale/)

!$omp atomic
      ntotshot=ntotshot+1
      resumnnllvint=0._dp
c--- ensure isolation code does not think this is fragmentation piece
      z_frag=0._dp
      
      W=sqrts**2
      p(:,:)=0._dp
      pjet(:,:)=0._dp

c---- set default reweight to 1 (hence no reweighting)
      reweight = 1.0_dp

      if (new_pspace) then
        call gen_lops(r,plo,pswt,*999)
!        call writeout(plo)
        npart=npart+1
        call multichan(r(ndim-2),r(ndim-1),r(ndim),
     &                 r(ndim+2),plo,p,pswtdip,*999)

        pswt=pswt*pswtdip
      else
        call gen_resumps(r,p,pswt,*999)
C        call gen_realps(r,p,pswt,*999)
      endif

      nvec=npart+2
      call dotem(nvec,p,s)

c--- (moved to includedipole) impose cuts on final state
c      if (kcase.ne.kvlchk6 .and. kcase.ne.ktautau) then
c        call masscuts(p,*999)
c      endif

      if (usescet) then
c----reject event if any tau is too small
        call smalltau(p,npart,*999)
      else
c---- reject event if any s(i,j) is too small
       call smalls(s,npart,*999)
      endif
c--- see whether this point will pass cuts - if it will not, do not
c--- bother calculating the matrix elements for it, instead bail out
      if (includedipole(0,p) .eqv. .false.) then
        goto 999
      endif

c      call writeout(p)
c      stop
      if (dynamicscale) call scaleset(initscale,initfacscale,p)
      
      xx(1)=-2._dp*p(1,4)/sqrts
      xx(2)=-2._dp*p(2,4)/sqrts

      z=r(ndim)**2
      xjac2=two*sqrt(z)
      omz=1._dp-z

      flux=fbGeV2/(2._dp*xx(1)*xx(2)*W)

c--- to test poles, we need colourchoice=0, but save real value
      if (nshot == 1) then
        rvcolourchoice=colourchoice
        colourchoice=0
      endif

   12 continue
c--- point to restart from when checking epsilon poles

c--- correction to epinv from AP subtraction when mu_FAC != mu_REN,
c--- corresponding to subtracting -1/epinv*Pab*log(musq_REN/musq_FAC)
      epcorr=epinv+2._dp*log(scale/facscale)

      AP(q,q,1)=+ason2pi*Cf*1.5_dp*epcorr
      AP(q,q,2)=+ason2pi*Cf*(-1._dp-z)*epcorr
      AP(q,q,3)=+ason2pi*Cf*2._dp/omz*epcorr
      AP(a,a,1)=+ason2pi*Cf*1.5_dp*epcorr
      AP(a,a,2)=+ason2pi*Cf*(-1._dp-z)*epcorr
      AP(a,a,3)=+ason2pi*Cf*2._dp/omz*epcorr

      AP(q,g,1)=0._dp
      AP(q,g,2)=ason2pi*Tr*(z**2+omz**2)*epcorr
      AP(q,g,3)=0._dp
      AP(a,g,1)=0._dp
      AP(a,g,2)=ason2pi*Tr*(z**2+omz**2)*epcorr
      AP(a,g,3)=0._dp

      AP(g,q,1)=0._dp
      AP(g,q,2)=ason2pi*Cf*(1._dp+omz**2)/z*epcorr
      AP(g,q,3)=0._dp
      AP(g,a,1)=0._dp
      AP(g,a,2)=ason2pi*Cf*(1._dp+omz**2)/z*epcorr
      AP(g,a,3)=0._dp

      AP(g,g,1)=+ason2pi*b0*epcorr
      AP(g,g,2)=+ason2pi*xn*2._dp*(1._dp/z+z*omz-2._dp)*epcorr
      AP(g,g,3)=+ason2pi*xn*2._dp/omz*epcorr

      do ia=-1,+1
      do ib=-1,+1
      do ic=-1,+1
      do is=1,3
        Q1(ia,ib,ic,is)=0._dp
        Q2(ia,ib,ic,is)=0._dp
      do cs=1,8
        H1(ia,ib,ic,cs,is)=0._dp
        H2(ia,ib,ic,cs,is)=0._dp
      enddo
      do cs=0,2
        R1(ia,ib,ic,cs,is)=0._dp
        R2(ia,ib,ic,cs,is)=0._dp
      do j=1,8
        S1(ia,ib,ic,j,cs,is)=0._dp
        S2(ia,ib,ic,j,cs,is)=0._dp
      enddo
      enddo
      enddo
      enddo
      enddo
      enddo
      M1(:,:,:,:,:)=zip
      M2(:,:,:,:,:)=zip
    
      QV = sqrt(2.0*dot(p,3,4))
      qt = pttwo(3,4,p)
      y = yraptwo(3,4,p)
      Lplus = p(3,4)+p(3,3)
      Lminus = p(3,4)-p(3,3)
      Pplus = p(4,4)+p(4,3)
      Pminus = p(4,4)-p(4,3)

      costheta = (Lplus*Pminus - Lminus*Pplus)/(Qv*sqrt(Qv*Qv+qt*qt))
      theta = acos(costheta)

c--- Calculate the required matrix elements      
      if     (kcase==kW_only) then
        call qqb_w(p,msq)
      elseif (kcase==kWgamma) then
        call qqb_wgam(p,msq)
      elseif (kcase==kZ_only) then
        resbos_result = resbosvar%mesq(Qv,qt,y,theta)
        if (qt > 2.0) then
            call qqb_z1jet(p,msq)
            call qqb_z1jet_v(p,msqv)
            call qqb_z1jet_z(p,z)
        endif
      elseif (kcase==kZgamma) then
        call set_anomcoup(p)
        call qqb_zgam_new(p,msq)
      elseif (kcase==kWWqqbr) then
        call qqb_ww(p,msq)
      elseif (kcase==kWZbbar) then
        call qqb_wz(p,msq)
      elseif (kcase==kZZlept) then
        call qqb_zz(p,msq)
      elseif (kcase==kggfus0) then
        call gg_h(p,msq)
      else
        write(6,*) 'Unimplemented process in resumnnllvint : kcase=',kcase
        stop 
      endif

      currentPDF=0

      xmsq = 0.0
      fx1z(:)=0._dp
      fx2z(:)=0._dp
      if (z > xx(1)) x1onz=xx(1)/z
      if (z > xx(2)) x2onz=xx(2)/z
      if(qt > 2.0) then
        call fdist(ih1,xx(1),facscale,fx1)
        call fdist(ih2,xx(2),facscale,fx2)
        if (z > xx(1)) then
          call fdist(ih1,x1onz,facscale,fx1z)
        endif
        if (z > xx(2)) then
          call fdist(ih2,x2onz,facscale,fx2z)
        endif

        do j=-nflav,nflav
        do k=-nflav,nflav
C--QQ
      if     ((j > 0) .and. (k>0)) then
      xmsq=xmsq+(msqv(j,k)
     & + msq(j,k)*(one+AP(q,q,1)-AP(q,q,3)+Q1(q,q,q,1)-Q1(q,q,q,3)
     &                +AP(q,q,1)-AP(q,q,3)+Q2(q,q,q,1)-Q2(q,q,q,3)))
     &                *fx1(j)*fx2(k)
     & +(msq(j,k)*(AP(q,q,2)+AP(q,q,3)+Q1(q,q,q,2)+Q1(q,q,q,3))
     & + msq(g,k)*(AP(g,q,2)+Q1(g,q,q,2)))*fx1z(j)/z*fx2(k)
     & +(msq(j,k)*(AP(q,q,2)+AP(q,q,3)+Q2(q,q,q,2)+Q2(q,q,q,3))
     & + msq(j,g)*(AP(g,q,2)+Q2(g,q,q,2)))*fx1(j)*fx2z(k)/z
C--QbarQbar
      elseif ((j < 0) .and. (k<0)) then
      xmsq=xmsq+(msqv(j,k)
     & + msq(j,k)*(one+AP(a,a,1)-AP(a,a,3)+Q1(a,a,a,1)-Q1(a,a,a,3)
     &                +AP(a,a,1)-AP(a,a,3)+Q2(a,a,a,1)-Q2(a,a,a,3)))
     &                *fx1(j)*fx2(k)
     & +(msq(j,k)*(AP(a,a,2)+AP(a,a,3)+Q1(a,a,a,2)+Q1(a,a,a,3))
     & + msq(g,k)*(AP(g,a,2)+Q1(g,a,a,2)))*fx1z(j)/z*fx2(k)
     & +(msq(j,k)*(AP(a,a,2)+AP(a,a,3)+Q2(a,a,a,2)+Q2(a,a,a,3))
     & + msq(j,g)*(AP(g,a,2)+Q2(g,a,a,2)))*fx1(j)*fx2z(k)/z
C--QQbar
      elseif ((j > 0) .and. (k<0)) then
      xmsq=xmsq+(msqv(j,k)
     & + msq(j,k)*(one+AP(q,q,1)-AP(q,q,3)+Q1(q,q,a,1)-Q1(q,q,a,3)
     &                +AP(a,a,1)-AP(a,a,3)+Q2(a,a,q,1)-Q2(a,a,q,3)))
     &                *fx1(j)*fx2(k)
     & +(msq(j,k)*(AP(q,q,2)+AP(q,q,3)+Q1(q,q,a,3)+Q1(q,q,a,2))
     & + msq(g,k)*(AP(g,q,2)+Q1(g,q,a,2)))*fx1z(j)/z*fx2(k)
     & +(msq(j,k)*(AP(a,a,2)+AP(a,a,3)+Q2(a,a,q,3)+Q2(a,a,q,2))
     & + msq(j,g)*(AP(g,a,2)+Q2(g,a,q,2)))*fx1(j)*fx2z(k)/z

      elseif ((j < 0) .and. (k>0)) then
C--QbarQ
      xmsq=xmsq+(msqv(j,k)
     & +msq(j,k)*(one+AP(a,a,1)-AP(a,a,3)+Q1(a,a,q,1)-Q1(a,a,q,3)
     &               +AP(q,q,1)-AP(q,q,3)+Q2(q,q,a,1)-Q2(q,q,a,3)))
     &               *fx1(j)*fx2(k)
     & +(msq(j,k)*(AP(a,a,3)+AP(a,a,2)+Q1(a,a,q,3)+Q1(a,a,q,2))
     & + msq(g,k)*(AP(g,a,2)+Q1(g,a,q,2)))*fx1z(j)/z*fx2(k)
     & +(msq(j,k)*(AP(q,q,3)+AP(q,q,2)+Q2(q,q,a,3)+Q2(q,q,a,2))
     & + msq(j,g)*(AP(g,q,2)+Q2(g,q,a,2)))*fx1(j)*fx2z(k)/z

      elseif ((j == g) .and. (k==g)) then
C--gg
       msq_qg=msq(+5,g)+msq(+4,g)+msq(+3,g)+msq(+2,g)+msq(+1,g)
     &       +msq(-5,g)+msq(-4,g)+msq(-3,g)+msq(-2,g)+msq(-1,g)
       msq_gq=msq(g,+5)+msq(g,+4)+msq(g,+3)+msq(g,+2)+msq(g,+1)
     &       +msq(g,-5)+msq(g,-4)+msq(g,-3)+msq(g,-2)+msq(g,-1)
       xmsq=xmsq+(msqv(g,g)
     &  +msq(g,g)*(one+AP(g,g,1)-AP(g,g,3)+Q1(g,g,g,1)-Q1(g,g,g,3)
     &                +AP(g,g,1)-AP(g,g,3)+Q2(g,g,g,1)-Q2(g,g,g,3)))
     &                *fx1(g)*fx2(g)
     &  +(msq(g,g)*(AP(g,g,2)+AP(g,g,3)+Q1(g,g,g,2)+Q1(g,g,g,3))
     &  +   msq_qg*(AP(q,g,2)+Q1(q,g,g,2)))*fx1z(g)/z*fx2(g)
     &  +(msq(g,g)*(AP(g,g,2)+AP(g,g,3)+Q2(g,g,g,2)+Q2(g,g,g,3))
     &  +   msq_gq*(AP(q,g,2)+Q2(q,g,g,2)))*fx1(g)*fx2z(g)/z
      elseif (j == g) then
C--gQ
       if    (k > 0) then
       msq_aq=msq(-1,k)+msq(-2,k)+msq(-3,k)+msq(-4,k)+msq(-5,k)
       msq_qq=msq(+1,k)+msq(+2,k)+msq(+3,k)+msq(+4,k)+msq(+5,k)
       xmsq=xmsq+(msqv(g,k)
     & +msq(g,k)*(one+AP(g,g,1)-AP(g,g,3)+Q1(g,g,q,1)-Q1(g,g,q,3)
     &               +AP(q,q,1)-AP(q,q,3)+Q2(q,q,g,1)-Q2(q,q,g,3)))
     &               *fx1(g)*fx2(k)
     & +(msq(g,k)*(AP(g,g,2)+AP(g,g,3)+Q1(g,g,q,2)+Q1(g,g,q,3))
     & +   msq_aq*(AP(a,g,2)+Q1(a,g,q,2))
     & +   msq_qq*(AP(q,g,2)+Q1(q,g,q,2)))*fx1z(g)/z*fx2(k)
     & +(msq(g,k)*(AP(q,q,2)+AP(q,q,3)+Q2(q,q,g,2)+Q2(q,q,g,3))
     & + msq(g,g)*(AP(g,q,2)+Q2(g,q,g,2)))*fx1(g)*fx2z(k)/z
C--gQbar
       elseif (k<0) then
       msq_qa=msq(+1,k)+msq(+2,k)+msq(+3,k)+msq(+4,k)+msq(+5,k)
       msq_aa=msq(-1,k)+msq(-2,k)+msq(-3,k)+msq(-4,k)+msq(-5,k)
       xmsq=xmsq+(msqv(g,k)
     & +msq(g,k)*(one+AP(g,g,1)-AP(g,g,3)+Q1(g,g,a,1)-Q1(g,g,a,3)
     &               +AP(a,a,1)-AP(a,a,3)+Q2(a,a,g,1)-Q2(a,a,g,3)))
     &               *fx1(g)*fx2(k)
     & +(msq(g,k)*(AP(g,g,2)+AP(g,g,3)+Q1(g,g,a,2)+Q1(g,g,a,3))
     & +   msq_qa*(AP(q,g,2)+Q1(q,g,a,2))
     & +   msq_aa*(AP(a,g,2)+Q1(a,g,a,2)))*fx1z(g)/z*fx2(k)
     & +(msq(g,k)*(AP(a,a,2)+AP(a,a,3)+Q2(a,a,g,2)+Q2(a,a,g,3))
     & + msq(g,g)*(AP(g,a,2)+Q2(g,a,g,2)))*fx1(g)*fx2z(k)/z
       endif
C--Qg
      elseif (k == g) then
       if     (j>0) then
       msq_qa=msq(j,-1)+msq(j,-2)+msq(j,-3)+msq(j,-4)+msq(j,-5)
       msq_qq=msq(j,+1)+msq(j,+2)+msq(j,+3)+msq(j,+4)+msq(j,+5)
       xmsq=xmsq+(msqv(j,g)
     & +msq(j,g)*(one
     &               +AP(q,q,1)-AP(q,q,3)+Q1(q,q,g,1)-Q1(q,q,g,3)
     &               +AP(g,g,1)-AP(g,g,3)+Q2(g,g,q,1)-Q2(g,g,q,3)))
     &               *fx1(j)*fx2(g)
     & +(msq(j,g)*(AP(q,q,2)+AP(q,q,3)+Q1(q,q,g,2)+Q1(q,q,g,3))
     & + msq(g,g)*(AP(g,q,2)+Q1(g,q,g,2)))*fx1z(j)/z*fx2(g)
     & +(msq(j,g)*(AP(g,g,2)+AP(g,g,3)+Q2(g,g,q,2)+Q2(g,g,q,3))
     & +   msq_qa*(AP(a,g,2)+Q2(a,g,q,2))
     & +   msq_qq*(AP(q,g,2)+Q2(q,g,q,2)))*fx1(j)*fx2z(g)/z
C--Qbarg
       elseif (j<0) then
       msq_aq=msq(j,+1)+msq(j,+2)+msq(j,+3)+msq(j,+4)+msq(j,+5)
       msq_aa=msq(j,-1)+msq(j,-2)+msq(j,-3)+msq(j,-4)+msq(j,-5)
       xmsq=xmsq+(msqv(j,g)
     & +msq(j,g)*(one+AP(a,a,1)-AP(a,a,3)+Q1(a,a,g,1)-Q1(a,a,g,3)
     &               +AP(g,g,1)-AP(g,g,3)+Q2(g,g,a,1)-Q2(g,g,a,3)))
     &                *fx1(j)*fx2(g)
     & +(msq(j,g)*(AP(a,a,2)+AP(a,a,3)+Q1(a,a,g,2)+Q1(a,a,g,3))
     & + msq(g,g)*(AP(g,a,2)+Q1(g,a,g,2)))*fx1z(j)/z*fx2(g)
     & +(msq(j,g)*(AP(g,g,2)+AP(g,g,3)+Q2(g,g,a,3)+Q2(g,g,a,2))
     & + msq_aq*(AP(q,g,2)+Q2(q,g,a,2))
     & + msq_aa*(AP(a,g,2)+Q2(a,g,a,2)))*fx1(j)*fx2z(g)/z
       endif
       endif
      if     (j > 0) then
        sgnj=+1
      elseif (j < 0) then
        sgnj=-1
      else
        sgnj=0
      endif
      if     (k > 0) then
        sgnk=+1
      elseif (k < 0) then
        sgnk=-1
      else
        sgnk=0
      endif

      enddo
      enddo
c--- code to check that epsilon poles cancel      
      if (nshot == 1) then
        if (xmsq == 0._dp) goto 999
        xmsq_old=xmsq
        nshot=nshot+1
        epinv=0._dp
        epinv2=0._dp
        goto 12
      elseif (nshot == 2) then
        nshot=nshot+1

        if (abs(xmsq_old/xmsq-1._dp) > 1.e-8_dp) then
!$omp master
          if (rank == 0) then
            write(6,*) 'epsilon fails to cancel'
            write(6,*) 'xmsq (epinv=large) = ',xmsq_old
            write(6,*) 'xmsq (epinv=zero ) = ',xmsq
            write(6,*) 'fractional difference',xmsq/xmsq_old-1d0
            call flush(6)
          endif
!$omp end master
C          stop
          colourchoice=rvcolourchoice
        else
!$omp master
          if (rank == 0) then
            write(6,*) 'Poles cancelled!'
c            write(6,*) 'xmsq (epinv=large) = ',xmsq_old
c            write(6,*) 'xmsq (epinv=zero ) = ',xmsq
c            write(6,*) 'fractional difference',xmsq/xmsq_old-1d0
            call flush(6)
          endif
!$omp end master
c          pause
          colourchoice=rvcolourchoice
        endif
      endif

      endif

C      resumnnllvint=flux*pswt*xmsq/BrnRat
C      if(resbos_result > 0 .and. qt < Q) then
C          print*, Q, qt, y, resbos_result*2.0*Q**4/xjac
C      endif
      M_T = sqrt(qt**2+Qv**2)
      Tkin = -M_T*sqrts*exp(-y)+Qv**2
      U = -M_T*sqrts*exp(y)+Qv**2
      x1Min = -U/(sqrts**2+Tkin-Qv**2)
      T1 = abs(xx(1)*sqrts**2+U-Qv**2)
      xjac = (1-x1Min)
      
      resbos_result = 1E3*resbos_result ! Convert from pb to fb
      resbos_result = resbos_result*512.0*pi**4*T1/xjac ! Remove real ps factors
      resbos_result = resbos_result*pi/2.0/(2.0*pi)**3/8.0/(pi*qt)/(2*Qv**2) !add in resbos factors
      if(qt > 2.0) then
      resumnnllvint = (resbos_result+xmsq*flux*xjac2)*pswt/BrnRat
      else
      resumnnllvint = (resbos_result)*pswt/BrnRat
      endif
c      resumnnllvint = (xmsq*flux*xjac2)*pswt/BrnRat
      
      call getptildejet(0,pjet)
      
      call dotem(nvec,pjet,s)

      val=resumnnllvint*wgt
      val2=val**2
      if(val.ne.val) then
!         write(6,*) 'resumnnllvint val = ',val
!         write(6,*) 'Discarding point with random variables',r
         resumnnllvint=zip
         val=zip
         goto 999
      endif
c---  SSbegin
      resumnnllvint = resumnnllvint*reweight
c---  SSend
c--- update the maximum weight so far, if necessary
c---  but not if we are already unweighting ...
      if ((.not.unweight) .and. (abs(val) > wtmax)) then
!$omp critical(MaxWgt)
        wtmax=abs(val)
!$omp end critical(MaxWgt)
      endif

      if (bin) then
c--- for EW corrections, make additional weight available inside common block
        if (kewcorr /= knone) then
          wt_noew=resumnnllvint_noew*wgt
        endif
        call nplotter(pjet,val,val2,0)
c---  POWHEG-style output if requested
        if (writepwg) then
!$omp critical(pwhgplot)
          call pwhgplotter(p,pjet,val,0)
!$omp end critical(pwhgplot)
        endif
      endif

c --- Check weights :
      if (unweight) then
c       write(6,*) 'Test weight ',val,' against max ',wtmax
        wtabs = abs(val)
c--- note that r(ndim+2) is reserved for new_pspace in real, so unused at LO
        if (r(ndim+2) < (wtabs/wtmax)) then
c         write(6,*) 'Keep event with weight',val
          if (wtabs<wtmax) then
            newwt = 1._dp
          else
            newwt = wtabs/wtmax
          endif
          if (newwt > 1.0_dp) then
            write(6,*) 'WARNING : resumnnllvint : event with |weight| > 1.',
     &                 ' |weight| = ',newwt
          endif
c ---     just in case the weight was negative :
          newwt = newwt*sign(one,val)
c         call nplotter(pjet,newwt,newwt,0)
!$omp critical(resumnnllvintWriteLHE)
          call mcfm_writelhe(pjet,xmsq_array,newwt)
!$omp end critical(resumnnllvintWriteLHE)
        endif
      endif

      return

 999  continue
      resumnnllvint=0._dp
!$omp atomic
      ntotzero=ntotzero+1
      
      return
      end



