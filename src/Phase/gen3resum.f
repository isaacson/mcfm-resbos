      subroutine gen3resum(r,p,wt3,*)
      implicit none
      include 'types.f'
c----generate 3 dimensional phase space weight and vectors p(7,4)
c----and x1 and x2 given seven random numbers
c----p(5,i) and p(4,i) are set equal to zero
c----
c---- if 'nodecay' is true, then the vector boson decay into massless
c---- particles is not included and 2 less integration variables
c---- are required
c---- 
c---- Generates dy_34 dqt^2 dQ^2 dphi_345 dcosTheta_34 dphi_34 dx1
c---- Based off of the variables used for analytic resummation
c---- Currently only works for massless p3 and p4
c---- Written by Josh Isaacson
      
      include 'constants.f'
      include 'nf.f'
      include 'mxpart.f'
      include 'cplx.h'
      include 'mxdim.f'
      include 'kprocess.f'
      include 'phasemin.f'
      include 'nodecay.f'
      include 'x1x2.f'
      include 'zerowidth.f'
      include 'limits.f'
      include 'breit.f'
      integer:: nu, j

      real(dp):: r(mxdim),wt3,rdk1,rdk2,
     & p(mxpart,4),p1(4),p2(4),p3(4),p4(4),p5(4),p6(4),p7(4),
     & p34(4),p12(4),px(4),py(4),pz(4)
      real(dp):: pswt,xjac,tau,y,taulow
      real(dp):: s3max,s3min,m3, s3, w3
      real(dp):: qtMax, qtMin, qtMax2, qtMin2, qt2, qt, wqt2
      real(dp):: ymax, YMaximum, yb, wty
      real(dp):: theSta, sThe, cThe, wtThe
      real(dp):: phiSta, sPhi, cPhi, wtPhi
      real(dp):: phi_345, wtPhi_345
      real(dp):: U, T, MT, T1, x1Min
      real(dp):: tm2Q, tmQ, ecml1, ecml2, pcml1
      include 'energy.f'

      wt3=0._dp
            
c--- dummy values if there's no decay
c      if (nodecay) then
c        rdk1=0.5_dp
c        rdk2=0.5_dp
c      else
c        rdk1=r(6)
c        rdk2=r(7)
c      endif

c--- First generate Q, qt, and y_34 (Needed to get x2)

c--- Generation of Q^2
      s3min=wsqmin
      s3max=sqrts**2
      call breitw(r(1),s3min,s3max,mass3,width3,s3,w3)
      m3=sqrt(s3)

c--- Generation of Qt^2
      qtMax = 0.5*sqrt(sqrts**2-4*s3)
      qtMin = 1e-2
      qtMax2 = qtMax**2
      qtMin2 = qtMin**2
      qt2 = qtMin2*(qtMax2/qtMin2)**r(2)
      qt = sqrt(qt2)
      wqt2 = qt2*log(qtMax2/qtMin2)

c--- Generation of y
      ymax = YMaximum(m3,qt)
      tau = s3/sqrts**2
      yb = ymax*(2.0*r(3)-1.0)
      wty = 2.0*ymax

c--- Generation of cosTheta_34 and phi_34
      theSta = acos(2.0*r(4)-1.0)
      phiSta = 2.0*pi*r(5)
      sThe = sin(theSta)
      cThe = cos(theSta)
      sPhi = sin(phiSta)
      cPhi = cos(phiSta)

      wtThe = 2.0
      wtPhi = 2.0*pi

c--- Generation of phi_345
      phi_345 = 2.0*pi*r(6)
      wtPhi_345 = 2.0*pi

c--- Generation of x1
      MT = sqrt(qt2+s3) 
      T = -MT*sqrts*exp(-yb)+s3
      U = -MT*sqrts*exp(yb)+s3

      x1Min = -U/(sqrts**2+T-s3)
      xx(1) = (1-x1Min)*r(7)+x1Min
      T1 = abs(xx(1)*sqrts**2+U-s3)
      xx(2) = (-s3-xx(1)*(T-s3))/T1
      xjac = (1-x1Min)

c--- for comparison with C. Oleari's e+e- --> QQbg calculation
c      if (runstring(1:5) == 'carlo') then
c        xx(1)=1._dp
c        xx(2)=1._dp
c        xjac=1._dp
c      endif

c--- phase space volume only checked for x1=x2=1
      if (kcase==kvlchwn) then
        xx(1)=1._dp
        xx(2)=1._dp
        xjac=1._dp
      endif

c---if x's out of normal range alternative return
      if   ((xx(1) >= 1._dp) 
     & .or. (xx(2) >= 1._dp)
     & .or. (xx(1) < xmin)
     & .or. (xx(2) < xmin)) return 1

      p1(4)=-xx(1)*sqrts*half
      p1(1)=zip
      p1(2)=zip
      p1(3)=-xx(1)*sqrts*half

      p2(4)=-xx(2)*sqrts*half
      p2(1)=zip
      p2(2)=zip
      p2(3)=+xx(2)*sqrts*half

      pswt=1.0/(1024.0*pi**5)*w3*wqt2*wty*wtThe*wtPhi*wtPhi_345
      if(pswt .lt. 0) then
        print*, pswt, w3, wqt2, wty, wtThe, wtPhi, wtPhi_345
        return 1
      endif

      pCMl1 = m3/2.0
      ECMl1 = m3/2.0
      ECMl2 = m3/2.0

      tm2Q = s3+qt2
      tmQ = sqrt(tm2Q)
      p34(4) = tmQ*cosh(yb)
      p34(1) = qt*cos(phi_345)
      p34(2) = qt*sin(phi_345)
      p34(3) = tmQ*sinh(yb)

      pX(4) = p34(4)*qt/tmQ/m3
      pX(1) = tmQ*cos(phi_345)/m3
      pX(2) = tmQ*sin(phi_345)/m3
      pX(3) = p34(3)*qt/tmQ/m3

      pZ(4) = p34(3)/tmQ
      pZ(1) = 0
      pZ(2) = 0
      pZ(3) = p34(4)/tmQ

      call GetYVect(m3,p34,pX,pZ,pY)

      do j=1,4
      p12(j) = -p1(j)-p2(j)
      p3(j)=ECMl1*p34(j)/m3+pCMl1*(sThe*cPhi*pX(j)+sThe*sPhi*pY(j)+cThe*pZ(j))
      p4(j)=p34(j)-p3(j)
      p5(j)=p12(j)-p3(j)-p4(j)
      enddo

      do nu=1,4
      p(1,nu)=p1(nu)
      p(2,nu)=p2(nu)
      p(3,nu)=p3(nu)
      p(4,nu)=p4(nu)
      p(5,nu)=p5(nu)
      p(6,nu)=p6(nu)
      p(7,nu)=p7(nu)
      enddo 
      wt3=xjac*pswt/T1

      if((wt3 == 0._dp) .or. (wt3 /= wt3)) then
      p(:,:)=0._dp
      return 1
      endif 

      return
      end

      function YMaximum(Q,qt)
      implicit none
      include 'types.f'
      real(dp):: Q, qt, YMaximum
      real(dp):: t1, t2, t3
      include 'energy.f'
      t1 = sqrts**2+Q**2
      t2 = sqrt(qt**2+q**2)
      t3 = 2.0*sqrts*t2

      YMaximum = acosh(t1/t3)
      if(YMaximum .ne. YMaximum) then
          YMaximum = 99
      endif

      end function

      function LeviCevita(i,j,k,l)
      implicit none
      integer:: i,j,k,l,LeviCevita
      LeviCevita = (i-j)*(i-k)*(i-l)*(j-k)*(j-l)*(k-l)/12
      end function

      subroutine GetYVect(Q, pQ, pX, pZ, pY)
      implicit none
      include 'types.f'
      integer:: i,j,k,l,LC,LeviCevita
      real(dp):: Q, pQ(4), pX(4), pZ(4), pY(4)
      pY(:) = 0.0
        do i=1,4
          do j=1,4
            do k=1,4
              do l=1,4
                LC = LeviCevita(i,j,k,l)
                if(LC.ne.0) then
                    pY(l) = pY(l) + pQ(i)*pX(j)*pZ(k)/Q*LC
                endif
              enddo
            enddo
          enddo
        enddo
      end subroutine
